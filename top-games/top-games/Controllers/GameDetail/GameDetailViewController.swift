//
//  GameDetailViewController.swift
//  top-games
//
//  Created by Rebecca Arantes Ferreira on 21/04/18.
//  Copyright © 2018 rbc.arantes. All rights reserved.
//

import UIKit

class GameDetailViewController: UIViewController {
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var gameName: UILabel!
    
    @IBOutlet weak var viewers: UILabel!
    @IBOutlet weak var popularity: UILabel!
    @IBOutlet weak var gameChannels: UILabel!
    
    var gameInfo: Game?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    func setupLayout() {
        guard let game = gameInfo else { return }
        gameName.text = game.gameName
        let gameTotalViewers: Int = game.gameViewers ?? 0
        viewers.text = "\(gameTotalViewers)"
        guard let url = game.gameImage else { return }
        gameImage.af_setImage(withURL: URL(string: url)!, placeholderImage: nil)
        let gamePop: Int = game.gamePropularity ?? 0
        popularity.text = "\(gamePop)"
        let gameChan: Int = game.gameChannels ?? 0
        gameChannels.text = "\(gameChan)"
    }
    
}
