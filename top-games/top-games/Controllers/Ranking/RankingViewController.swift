//
//  RankingViewController.swift
//  top-games
//
//  Created by Rebecca Arantes Ferreira on 21/04/18.
//  Copyright © 2018 rbc.arantes. All rights reserved.
//

import UIKit

class RankingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: RankingViewModel?
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTable()
        setupViewModel()
    }
    
    func configTable() {
        tableView.register(cellType: GameCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50.0
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setupViewModel() {
        viewModel = RankingViewModel.newWithFeedback(rankingFeedback: self)
        viewModel?.loadGames()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc1 = segue.destination as? GameDetailViewController {
            vc1.gameInfo = viewModel?.gameForIndex(index)
        }
    }
}

extension RankingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getGames().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let gameCard: Game = viewModel?.gameForIndex(indexPath.row) {
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: GameCell.self)
            cell.configCell(gameInfo: gameCard, rankingPosition: indexPath.row + 1)
                //                cell.delegate = self
                cell.selectionStyle = .none
                return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    // Pagination
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let count = viewModel?.getGames().count {
            if (count - 5 == indexPath.row) {
                viewModel?.loadGames()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
        self.index = indexPath.row
        performSegue(withIdentifier: "goToDetail", sender: cell)
    }
}

extension RankingViewController: RankingFeedback {
    func didLoadGames() {
        tableView.reloadData()
    }
}
