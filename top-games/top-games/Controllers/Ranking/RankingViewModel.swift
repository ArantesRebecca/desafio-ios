//
//  RankingViewModel.swift
//  top-games
//
//  Created by Rebecca Arantes Ferreira on 21/04/18.
//  Copyright © 2018 rbc.arantes. All rights reserved.
//

import Foundation
import UIKit

protocol RankingFeedback: class {
    func didLoadGames()
}

protocol RankingDataSource {
    func gameForIndex(_ index: Int) -> Game?
    func getGames() -> [Game]
}

protocol RankingDelegate {
    func loadGames()
}

class RankingViewModel: RankingDataSource {
    
    var gamesList = [Game]()
    var rankingFeedback: RankingFeedback?
    var page: Int = 0
    
    class func newWithFeedback(rankingFeedback: RankingFeedback) -> RankingViewModel {
        let instance = RankingViewModel()
        instance.rankingFeedback = rankingFeedback
        return instance
    }
    
    func gameForIndex(_ index: Int) -> Game? {
        if index < gamesList.count {
            return gamesList[index]
        } else {
            return nil
        }
    }
    
    func getGames() -> [Game] {
        return gamesList
    }
}


extension RankingViewModel: RankingDelegate {
    func loadGames() {
        GamesService.makeRequest(page: page, onSuccess: { (result) in
            self.page = self.page + 10 // increment page number by 10
            guard let result = result.top else { return }
            for game in result {
                self.gamesList.append(game)
            }
            self.rankingFeedback?.didLoadGames()
        })
        
    }
}
