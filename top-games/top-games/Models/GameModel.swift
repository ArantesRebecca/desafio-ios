//
//  GameModel.swift
//  top-games
//
//  Created by Rebecca Arantes Ferreira on 21/04/18.
//  Copyright © 2018 rbc.arantes. All rights reserved.
//

import Foundation
import ObjectMapper

class Game: Mappable {
    
    var gameName: String?
    var gameImage: String?
    var gameViewers: Int?
    var gamePropularity: Int?
    var gameLogo: String?
    var gameChannels: Int?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        gameName          <- map["game.name"]
        gameImage         <- map["game.box.medium"]
        gameViewers       <- map["viewers"]
        gamePropularity   <- map["game.popularity"]
        gameLogo          <- map["game.logo.medium"]
        gameChannels      <- map["channels"]
    }
}

class Top: Mappable{
    
    var top: [Game]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        top          <- map["top"]
    }
}
