//
//  GamesService.swift
//  top-games
//
//  Created by Rebecca Arantes Ferreira on 21/04/18.
//  Copyright © 2018 rbc.arantes. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class GamesService {
    static func makeRequest(page: Int, onSuccess success: @escaping (_ result: Top) -> Void) {
        let headers: HTTPHeaders = [
            "Client-Id": "42b2xzwhgyrmhu3g355lmkgu084bui"
        ]
        
        Alamofire.request("https://api.twitch.tv/kraken/games/top?offset=" + String(page), headers: headers).responseObject { (response: DataResponse<Top>) in
            let top = response.result.value
            success(top!)
        }
    }
    
}



