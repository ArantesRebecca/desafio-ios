//
//  GameCell.swift
//  top-games
//
//  Created by Rebecca Arantes Ferreira on 21/04/18.
//  Copyright © 2018 rbc.arantes. All rights reserved.
//

import UIKit
import Reusable
import AlamofireImage

//protocol GameCellDelegate {
//    let destination = GameDetailViewController()
//    navigationController?.pushViewController(destination, animated: true)
//}

class GameCell: UITableViewCell, NibReusable  {

    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var totalViews: UILabel!
    @IBOutlet weak var rankingPlace: UILabel!
    @IBOutlet weak var gameCover: UIImageView!
   
    var gameCard: Game?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(gameInfo: Game, rankingPosition: Int) {
        self.gameCard = gameInfo
        guard let gameCard = gameCard else { return }
        gameName.text = gameCard.gameName
        let gameTotalViewers: Int = gameCard.gameViewers ?? 0
        totalViews.text = "\(gameTotalViewers)" + " viewers"
        guard let url = gameCard.gameImage else { return }
        gameCover.af_setImage(withURL: URL(string: url)!, placeholderImage: nil)
        rankingPlace.text = "\(rankingPosition)"
    }
    
}
